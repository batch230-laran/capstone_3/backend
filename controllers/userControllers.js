


const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



 
// ------------------------- USER REGISTRATION
 

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
		isAdmin: reqBody.isAdmin
		})
	
	return newUser.save().then((user, error) => {
		if(error){
			return ("Duplicate user found")
		}
		else{
			return ("New user registered")
		}
	})
}


// ------------------------- Check Email if Existing

module.exports.checkEmailExist = (reqBody) =>{
		return User.find({email: reqBody.email}).then(result =>{
				if(result.length > 0){
					return true;
				}
				else{
					return false;
				}
		}
	)
}






// ------------------------- USER AUTHENTICATION


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return false;
			}
		}
	})
}
 




// ------------------------- RETRIEVE USER DETAILS USING JWT




module.exports.userDetails = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	console.log(userData);


	return User.findById(userData.id).then(result => {
		result.password = "******";
		response.send(result);
		
	})
}





// ------------------------- CHECK OUT / CREATE ORDER


module.exports.order = async (request, response) => {
	
	const userData = auth.decode(request.headers.authorization);

	Product.findById(request.body.productId).then(async result =>{ 
		if(result.stocks <= 0){
			console.log(`Stocks available ${result.stocks}`);
			return response.send(false);
		}
		else if(result.stocks < request.body.quantity){
			console.log(`Stocks available ${result.stocks}`);
			return response.send(false)
		}
		else{
	
			let productName = await Product.findById(request.body.productId).then(result => result.name);

			let newData = {
				userId: userData.id,
				email: userData.email,
				productId: request.body.productId,
				quantity: request.body.quantity,
				productName: productName
			}

			let productPrice = await Product.findById(request.body.productId).then(result => result.price);
			let priceValue = parseInt(productPrice);

			console.log("thisIsTheNewDataObject");
			console.log(newData);

			let isUserUpdated = await User.findById(newData.userId)
				.then (user => {
					user.orders.push({
						totalAmount: parseInt(priceValue * request.body.quantity),
						products: [
							{
								productId: newData.productId,
								productName: newData.productName,
								quantity: newData.quantity
							}
						]
					});

					return user.save();
				})
				.then(result => {
					console.log(result);
					return true;
				})
				.catch(error => {
					console.log(error);
					return false;
				});
			
			console.log(isUserUpdated);


			let isProductUpdated = await Product.findById(newData.productId).then(product => {
				product.orders.push({
					userId: newData.userId,
					email: newData.userEmail,
					quantity: newData.quantity

				})

				product.stocks = product.stocks - request.body.quantity;

					return product.save()
					.then(result => {
						console.log(result);
						return true;
					})
					.catch(error => {
						console.log(error);
						return false;
					})
			})

			console.log(isProductUpdated);



			(isUserUpdated == true && isProductUpdated == true)?
				response.send(true) : response.send(false);

		}
	})
		
}





