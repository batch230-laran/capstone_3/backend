

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const port = 4000;
const app = express();
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");


app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());
app.use("/users", userRoutes);
app.use("/products", productRoutes);


mongoose.connect("mongodb+srv://admin:admin@batch230.gwcbkyy.mongodb.net/LaranShieraEcommerce?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	})	

mongoose.connection.once("open", () => console.log("Now connected to Laran-Mongo DB Atlas"));

app.listen(port, () => console.log(`Capstone 3 Server is runnig at port ${port}`));


