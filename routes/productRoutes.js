const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");



// ------------------------- CREATE PRODUCT (ADMIN ONLY)

router.post("/create", auth.verify, (request, response) => {
	const addedProduct = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	productControllers.addProduct(request.body, addedProduct).then(
		resultFromController => response.send(resultFromController))
})




// ------------------------- RETRIEVE ALL ACTIVE PRODUCTS

router.get("/active", (request, response) => {
	productControllers.getActiveProducts().then(resultFromController => response.send(resultFromController))
})



// ------------------------- RETRIEVE ALL PRODUCTS
router.get("/all", (request, response) => {
	productControllers.getAllProducts().then(resultFromController => response.send(resultFromController))
})




// ------------------------- RETRIEVE SINGLE PRODUCT

router.get("/:productId", (request, response) => {
	productControllers.getProduct(request.params.productId).then(resultFromController => response.send(resultFromController))
})





// ------------------------- UPDATE PRODUCT INFORMATION (ADMIN ONLY)

router.patch("/:productId/update", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.updateProduct(request.params.productId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})




// ------------------------- ARCHIEVE PRODUCT (ADMIN ONLY)

router.patch("/:productId/archive", auth.verify, (request,response) => 
{
	const archiveData = {
		product: request.body, 	
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.archiveProduct(request.params.productId, archiveData).then(resultFromController => {
		response.send(resultFromController)
	})
})




module.exports = router;

